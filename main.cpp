/*
    Author: Tanvir Alam
    tanviralvis@gmail.comm
*/


#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif

#include<iostream>
#include <tchar.h>
#include<stdio.h>
#include<string.h>
#include <windows.h>
#include <winioctl.h>
#define BUTTON_PRESS 4
#define HDD_BUTTON_PRESS 5
#define GPU_BUTTON_PRESS 6
#include <vector>


int butt=0,butt2=0,butt3=0,gpuflag=0;
double hddsz=0;
/*  Declare Windows procedure  */
char text[128];
char text2[128];

#define UNICODE 1
#define _UNICODE 1

/* The code of interest is in the subroutine GetDriveGeometry. The
   code in main shows how to interpret the results of the call. */


#include <winioctl.h>


#define wszDrive L"\\\\.\\PhysicalDrive0"

#define NVAPI_MAX_PHYSICAL_GPUS   64
#define NVAPI_MAX_USAGES_PER_GPU  34

// function pointer types
typedef int *(*NvAPI_QueryInterface_t)(unsigned int offset);
typedef int (*NvAPI_Initialize_t)();
typedef int (*NvAPI_EnumPhysicalGPUs_t)(int **handles, int *count);
typedef int (*NvAPI_GPU_GetUsages_t)(int *handle, unsigned int *usages);

int gpuinformation()
{
    HMODULE hmod = LoadLibraryA("nvapi.dll");
    if (hmod == NULL)
    {
        std::cerr << "Couldn't find nvapi.dll" << std::endl;
        return 1;
    }

    // nvapi.dll internal function pointers
    NvAPI_QueryInterface_t      NvAPI_QueryInterface     = NULL;
    NvAPI_Initialize_t          NvAPI_Initialize         = NULL;
    NvAPI_EnumPhysicalGPUs_t    NvAPI_EnumPhysicalGPUs   = NULL;
    NvAPI_GPU_GetUsages_t       NvAPI_GPU_GetUsages      = NULL;

    // nvapi_QueryInterface is a function used to retrieve other internal functions in nvapi.dll
    NvAPI_QueryInterface = (NvAPI_QueryInterface_t) GetProcAddress(hmod, "nvapi_QueryInterface");

    // some useful internal functions that aren't exported by nvapi.dll
    NvAPI_Initialize = (NvAPI_Initialize_t) (*NvAPI_QueryInterface)(0x0150E828);
    NvAPI_EnumPhysicalGPUs = (NvAPI_EnumPhysicalGPUs_t) (*NvAPI_QueryInterface)(0xE5AC921F);
    NvAPI_GPU_GetUsages = (NvAPI_GPU_GetUsages_t) (*NvAPI_QueryInterface)(0x189A1FDF);

    if (NvAPI_Initialize == NULL || NvAPI_EnumPhysicalGPUs == NULL ||
        NvAPI_EnumPhysicalGPUs == NULL || NvAPI_GPU_GetUsages == NULL)
    {
        std::cerr << "Couldn't get functions in nvapi.dll" << std::endl;
        return 2;
    }

    // initialize NvAPI library, call it once before calling any other NvAPI functions
    (*NvAPI_Initialize)();

    int          gpuCount = 0;
    int         *gpuHandles[NVAPI_MAX_PHYSICAL_GPUS] = { NULL };
    unsigned int gpuUsages[NVAPI_MAX_USAGES_PER_GPU] = { 0 };

    // gpuUsages[0] must be this value, otherwise NvAPI_GPU_GetUsages won't work
    gpuUsages[0] = (NVAPI_MAX_USAGES_PER_GPU * 4) | 0x10000;

    (*NvAPI_EnumPhysicalGPUs)(gpuHandles, &gpuCount);

    // print GPU usage every second
    for (int i = 0; i < 5; i++)
    {
        (*NvAPI_GPU_GetUsages)(gpuHandles[0], gpuUsages);
        int usage = gpuUsages[3];
        std::cout << "GPU Usage: " << usage << std::endl;
       // Sleep(100);
        if(usage==1)
            gpuflag=1;
    }
    if(gpuflag==0)
    sprintf(text2, "No Gpu Used","");
    else
    sprintf(text2, "Gpu Used");

    return 0;
}
/**********/
BOOL GetDriveGeometry(LPWSTR wszPath, DISK_GEOMETRY *pdg)
{
  HANDLE hDevice = INVALID_HANDLE_VALUE;  // handle to the drive to be examined
  BOOL bResult   = FALSE;                 // results flag
  DWORD junk     = 0;                     // discard results

  hDevice = CreateFileW(wszPath,          // drive to open
                        0,                // no access to the drive
                        FILE_SHARE_READ | // share mode
                        FILE_SHARE_WRITE,
                        NULL,             // default security attributes
                        OPEN_EXISTING,    // disposition
                        0,                // file attributes
                        NULL);            // do not copy file attributes

  if (hDevice == INVALID_HANDLE_VALUE)    // cannot open the drive
  {
    return (FALSE);
  }

  bResult = DeviceIoControl(hDevice,                       // device to be queried
                            IOCTL_DISK_GET_DRIVE_GEOMETRY, // operation to perform
                            NULL, 0,                       // no input buffer
                            pdg, sizeof(*pdg),            // output buffer
                            &junk,                         // # bytes returned
                            (LPOVERLAPPED) NULL);          // synchronous I/O

  CloseHandle(hDevice);

  return (bResult);
}



int hddsize()
{
  DISK_GEOMETRY pdg = { 0 }; // disk drive geometry structure
  BOOL bResult = FALSE;      // generic results flag
  ULONGLONG DiskSize = 0;    // size of the drive, in bytes

  bResult = GetDriveGeometry (wszDrive, &pdg);

  if (bResult)
  {
    wprintf(L"Drive path      = %ws\n",   wszDrive);
    wprintf(L"Cylinders       = %I64d\n", pdg.Cylinders);
    wprintf(L"Tracks/cylinder = %ld\n",   (ULONG) pdg.TracksPerCylinder);
    wprintf(L"Sectors/track   = %ld\n",   (ULONG) pdg.SectorsPerTrack);
    wprintf(L"Bytes/sector    = %ld\n",   (ULONG) pdg.BytesPerSector);

    DiskSize = pdg.Cylinders.QuadPart * (ULONG)pdg.TracksPerCylinder *
               (ULONG)pdg.SectorsPerTrack * (ULONG)pdg.BytesPerSector;
    wprintf(L"Disk size       = %I64d (Bytes)\n"
            L"                = %.2f (Gb)\n",
            DiskSize, (double) DiskSize / (1024 * 1024 * 1024));
    hddsz= DiskSize ;

    sprintf(text, "Size: %I64d (Bytes)\nHDD Size: %.2f (GB)",DiskSize, (double) DiskSize / (1024 * 1024 * 1024));
  //  MessageBox(NULL, text, "", MB_OK);
  }
  else
  {
    wprintf (L"GetDriveGeometry failed. Error %ld.\n", GetLastError ());
  }

  return ((int)bResult);
}

LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
TCHAR szClassName[ ] = _T("CodeBlocksWindowsApp");





void getPSN(char *PSN)
{///cpu serial no
int varEAX, varEBX, varECX, varEDX;
char str[9];
//%eax=1 gives most significant 32 bits in eax
__asm__ __volatile__ ("cpuid": "=a" (varEAX), "=b" (varEBX), "=c" (varECX), "=d" (varEDX) : "a" (1));
sprintf(str, "%08X", varEAX); //i.e. XXXX-XXXX-xxxx-xxxx-xxxx-xxxx
sprintf(PSN, "%C%C%C%C-%C%C%C%C", str[0], str[1], str[2], str[3], str[4], str[5], str[6], str[7]);
//%eax=3 gives least significant 64 bits in edx and ecx [if PN is enabled]
__asm__ __volatile__ ("cpuid": "=a" (varEAX), "=b" (varEBX), "=c" (varECX), "=d" (varEDX) : "a" (3));
sprintf(str, "%08X", varEDX); //i.e. xxxx-xxxx-XXXX-XXXX-xxxx-xxxx
sprintf(PSN, "%s-%C%C%C%C-%C%C%C%C", PSN, str[0], str[1], str[2], str[3], str[4], str[5], str[6], str[7]);
sprintf(str, "%08X", varECX); //i.e. xxxx-xxxx-xxxx-xxxx-XXXX-XXXX
sprintf(PSN, "%s-%C%C%C%C-%C%C%C%C", PSN, str[0], str[1], str[2], str[3], str[4], str[5], str[6], str[7]);
}

void AddControls(HWND hWnd);
int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           _T("Code::Blocks Template Windows App"),       /* Title Text */
           WS_OVERLAPPEDWINDOW, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           744,                 /* The programs width */
           375,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    ///MessageBox(NULL,"Hello Dhaka","My first GUI",MB_OK);
    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)                  /* handle the messages */
    {
    case WM_COMMAND:
        switch(wParam)
        {
            case BUTTON_PRESS:
              //  if(butt==0)
                    butt=1;
//                else
//                    butt=0;
//                std:: cout <<butt;
                AddControls(hwnd);
//               std:: cout <<butt;
                break;
            case HDD_BUTTON_PRESS:
                butt2=1;
                 std:: cout <<butt2;
                break;
            case GPU_BUTTON_PRESS:
                butt3=1;
                break;

        }
        case WM_CREATE:


            AddControls(hwnd);
            break;
        case WM_DESTROY:
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;

        default:                      /* for messages that we don't deal with */
            return DefWindowProc (hwnd, message, wParam, lParam);
    }
   // AddControls(hwnd);
    return 0;
}

void AddControls(HWND hWnd)
{

   // CreateWindowW(L"static",L"Enter text here",WS_VISIBLE|WS_CHILD|SS_CENTER,200,100,100,50,hWnd,NULL,NULL,NULL);
    CreateWindowW(L"Button",L"CPU serial No. : ",WS_VISIBLE|WS_CHILD|SS_CENTER,00,00,200,50,hWnd,(HMENU)BUTTON_PRESS,NULL,NULL);
    CreateWindowW(L"Button",L"HDD info. : ",WS_VISIBLE|WS_CHILD|SS_CENTER,00,60,200,50,hWnd,(HMENU)HDD_BUTTON_PRESS,NULL,NULL);
    CreateWindowW(L"Button",L"GPU info. : ",WS_VISIBLE|WS_CHILD|SS_CENTER,00,120,200,50,hWnd,(HMENU)GPU_BUTTON_PRESS,NULL,NULL);
    if(butt==1)
    {
        //std:: cout<<butt<<"Hello";
        ///CPU serial no
            char PSN[30]; //24 Hex digits, 5 '-' separators, and a '\0'
            getPSN(PSN);
            printf("%s\n", PSN); //compare with: lshw | grep serial:
            ///CPU serial no
           // const TCHAR* data = TEXT(PSN);
           const TCHAR* data = TEXT(PSN);    // LPCTSTR


            CreateWindowW(L"static",L"CPU Serial no. : ",WS_VISIBLE|WS_CHILD|SS_CENTER,200,00,120,50,hWnd,NULL,NULL,NULL);
            CreateWindow(TEXT("STATIC"), data, WS_VISIBLE | WS_CHILD |
                WS_BORDER,320, 0,300,50,hWnd,NULL, NULL, NULL);
//        CreateWindowW(TEXT("STATIC"),data,WS_VISIBLE|WS_CHILD|SS_CENTER,00,00,100,50,hWnd,NULL,NULL,NULL);
        //SetWindowText(PSN, "loooo");
    }
     if(butt2==1)
    {

         hddsize();

        const TCHAR* data2 = TEXT(text);

         CreateWindow(TEXT("STATIC"), data2, WS_VISIBLE | WS_CHILD |
              WS_BORDER,200, 60,300,50,hWnd,NULL, NULL, NULL);
    }
    if(butt3==1){

        gpuinformation();
         const TCHAR* data3 = TEXT(text2);

         CreateWindow(TEXT("STATIC"), data3, WS_VISIBLE | WS_CHILD |
              WS_BORDER,200, 120,300,50,hWnd,NULL, NULL, NULL);
    }
    else{
       // CreateWindowW(L"static",L"Enter text here asdhkj",WS_VISIBLE|WS_CHILD|SS_CENTER,200,100,100,50,hWnd,NULL,NULL,NULL);
    }
}

