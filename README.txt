This is a simple Visual C++ based GUI project with win32 web api.

We can get some basic information of windows (Specially win10) by this project.
For example- CPU information, HDD information, Gpu information of a system.

In this project a window contains 3 buttons called CPU serial no., HDD info, GPU info.
by clicking these buttons we get the specific information.also some information was printed in the console because
all that informations were not important I thought.

Most of the resources were collected from internet (mainly stackoverflow).

# Methods Info.:
 
Mainly 3 buttons coresponding methods are.
- gpuinformation()
- getPSN
- hddsize()


# Variables Information: 

int butt=0,butt2=0,butt3=0 these variables were used for detecting which button was pressed.  
char text[128], text2[128]; these variables were used for taking data and show it in gui window.

# nvapi.dll file was imported

For getting the gpu information nvapi.dll file was imported.

if you run PcConfig.exe file it will also work fine.which file you can get from bin/Debug folder.you can share only this file to other.
this project was developed by using Code::Blocks IDE.
 